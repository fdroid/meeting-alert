import os
import time

from datetime import datetime

import requests
from ruamel import yaml

MATRIX_ROOM = os.environ['MATRIX_ROOM']
MATRIX_ACCESS_TOKEN = os.environ['MATRIX_ACCESS_TOKEN']

MEETING_ID = int(os.environ['MEETING_ID'])
MEETING_EXTRA = os.environ.get('MEETING_EXTRA')

helpers = {
    "week_is_even": lambda: datetime.today().isocalendar().week % 2 == 0,
    "week_is_uneven": lambda: datetime.today().isocalendar().week % 2 == 1
}

# Get meeting info
with open("meetings.yml", "r") as f:
    meetings = yaml.safe_load(f)

    # 1-index the meetings.yml
    meeting = meetings[MEETING_ID - 1]

    if MEETING_EXTRA:
        if not helpers[meeting['extra'][MEETING_EXTRA]]():
           print("Skipping notification, extra was not met")
           exit(0)

    if 'ping' in meeting and len(meeting['ping']) > 0:
        message = f"{' '.join(meeting['ping'])}: "
    else:
        message = ""

    message = f"{message}{meeting['message']}"

r = requests.put(
        f"https://matrix.org/_matrix/client/r0/rooms/{MATRIX_ROOM}/send/m.room.message/{int(time.time())}",
        headers = {
            "Authorization": f"Bearer {MATRIX_ACCESS_TOKEN}",
            "Accept": "application/json"
        },
        json = {
            "msgtype": "m.text",
            "body": message
        }
    )
r.raise_for_status()
