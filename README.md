# F-Droid Meeting Alert

A simple GitLab CI pipeline that pings for the F-Droid meetings in the F-Droid Dev channel.

If you want to be alerted, make a PR to add your name to the meeting in `meetings.yml`.

If you want to have another scheduled meeting, make an issue and we can discuss it with the team and add it.

## Setup

The account that runs the scheduled jobs needs to have certain [GitLab permissions](https://docs.gitlab.com/ee/user/permissions.html):

* _Developer_ or higher to run a CI/CD pipeline.
* Permission to merge to the [protected branch](https://gitlab.com/fdroid/meeting-alert/-/settings/repository#js-protected-branches-settings) (e.g. _master_) to read the variables.
* Any tokens needed to post to things, like Matrix, should be set in the [CI/CD Variables](https://gitlab.com/fdroid/meeting-alert/-/settings/ci_cd#js-cicd-variables-settings)

## License

This program is Free Software: You can use, study share and improve it at your
will. Specifically you can redistribute and/or modify it under the terms of the
[GNU General Public License](https://www.gnu.org/licenses/gpl.html) as
published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.